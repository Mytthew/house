package com.company;

public class Main {

    public static void main(String[] args) {
		House house = new House();
		house.add(new Fridge("Fridge", "LG", "black", 260, "Kitchen", 190, 90, 65));
		house.add(new Washer("Washer", "Samsung", "white", 150, "Bathroom", 1600));
		house.add(new Bulb("Bulb", "SRAM", "White", 20, "Everywhere", 220));
		System.out.println(house);
		System.out.println("Power consumption of this house = " + house.powerConsumption());
    }
}
