package com.company;

public class Washer extends Device {
	private int rotationPerMin;

	public Washer(String type, String producer, String color, int power, String place, int rotationPerMin) {
		super(type, producer, color, power, place);
		this.rotationPerMin = rotationPerMin;
	}

	public int getRotationPerMin() {
		return rotationPerMin;
	}

	@Override
	public String toString() {
		return "Washer{" +
				"rotationPerMin=" + rotationPerMin +
				'}';
	}
}
