package com.company;

public class Device {
	private final String type;
	private final String producer;
	private final String color;
	private final int power;
	private final String place;

	public Device(String type, String producer, String color, int power, String place) {
		this.type = type;
		this.producer = producer;
		this.color = color;
		this.power = power;
		this.place = place;
	}

	public String getType() {
		return type;
	}

	public String getProducer() {
		return producer;
	}

	public String getColor() {
		return color;
	}

	public int getPower() {
		return power;
	}

	@Override
	public String toString() {
		return "Device{" +
				"type='" + type + '\'' +
				", producer='" + producer + '\'' +
				", color='" + color + '\'' +
				", power=" + power +
				", place='" + place + '\'' +
				'}';
	}
}
