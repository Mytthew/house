package com.company;

import java.util.ArrayList;
import java.util.List;

public class House {
	private List<Device> deviceList = new ArrayList<>();

	public List<Device> getDeviceList() {
		return deviceList;
	}

	public void add(Device device) {
		deviceList.add(device);
	}

	public int powerConsumption() {
		int sumOfPowerConsumption = 0;
		for (int i = 0; i < deviceList.size(); i++) {
			sumOfPowerConsumption += deviceList.get(i).getPower();
		}
		return sumOfPowerConsumption;
	}

	@Override
	public String toString() {
		return "House{" +
				"deviceList=" + deviceList +
				'}';
	}
}
