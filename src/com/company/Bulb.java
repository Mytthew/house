package com.company;

public class Bulb extends Device {
	private int lumens;

	public Bulb(String type, String producer, String color, int power, String place, int lumens) {
		super(type, producer, color, power, place);
		this.lumens = lumens;
	}

	public int getLumens() {
		return lumens;
	}

	@Override
	public String toString() {
		return "Bulb{" +
				"lumens=" + lumens +
				'}';
	}
}
