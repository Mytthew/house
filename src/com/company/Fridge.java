package com.company;

public class Fridge extends Device {
	private int height;
	private int width;
	private int depth;

	public Fridge(String type, String producer, String color, int power, String place, int height, int width, int depth) {
		super(type, producer, color, power, place);
		this.height = height;
		this.width = width;
		this.depth = depth;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public int getDepth() {
		return depth;
	}

	@Override
	public String toString() {
		return "Fridge{" +
				"height=" + height +
				", width=" + width +
				", depth=" + depth +
				'}';
	}
}
